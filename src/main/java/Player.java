import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.time.LocalDate;

public record Player(String alias, LocalDate lastMatch, int scorePerMinute, int wins, int looses) implements Comparable<Player> {

    public static Player createPlayer(byte[] b) throws IOException {
        try (DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(b))) {
            String aliased = dataInputStream.readUTF();
            int lastMatchYear = dataInputStream.readInt();
            int lastMatchMonth = dataInputStream.readInt();
            int lastMatchDay = dataInputStream.readInt();
            int scorePerMinute = dataInputStream.readInt();
            int wins = dataInputStream.readInt();
            int looses = dataInputStream.readInt();
            return new Player(aliased, LocalDate.of(lastMatchYear, lastMatchMonth, lastMatchDay), scorePerMinute, wins, looses);
        } catch (IOException e) {
            throw new IOException(e.getLocalizedMessage());
        }
    }
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("Alias " + alias() + "\n" +
                "Last Match " + lastMatch().toString() + "\n" +
                "Score per minute " + scorePerMinute() + "\n" +
                "Wins " + wins() + "\n" +
                "Losses " + looses() + "\n" +
                "-------------------------\n");
        return s.toString();
    }

    @Override
    public int compareTo(Player that) {
        if (this.scorePerMinute != that.scorePerMinute) {
            return (this.scorePerMinute > that.scorePerMinute ? -1 : 1);
        }

        int aliasComparison = this.alias.compareTo(that.alias);
        if (aliasComparison != 0) {
            return aliasComparison < 0 ? -1 : 1;
        }

        if (this.wins != that.wins) {
            return (this.wins < that.wins ? -1 : 1);
        }

        if (this.looses != that.looses) {
            return (this.looses < that.looses ? -1 : 1);
        }

        return 0;
    }
}
