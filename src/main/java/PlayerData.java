
import javax.xml.crypto.Data;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PlayerData {

    private List<Player> playerList = new ArrayList<>();



    public List<Player> readData(String filename) throws IOException {
        this.playerList = new ArrayList<>();
        try (DataInputStream dis = new DataInputStream(new FileInputStream(filename))) {
            dis.skipBytes(1345);
            short playercount = dis.readShort();
            for (int i = 0; i < playercount; i++) {
                short playerDataSize = dis.readShort();
                byte[] bytes = new byte[playerDataSize+2];
                dis.read(bytes);
                this.playerList.add(Player.createPlayer(bytes));
            }

        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getLocalizedMessage());
        } catch (IOException e) {
            throw new IOException(e.getLocalizedMessage());
        }
        this.playerList.sort(Player::compareTo);
        return this.playerList;
    }

    public void findPlayer(String alias, int newScorePerMinute, int newWins, int newLosses) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile("src/main/resources/playerdata.bin", "rw")){
            raf.skipBytes(1345);
            short playercount = raf.readShort();
            for (int i = 0; i < playercount; i++) {
                raf.skipBytes(2);
                String aliased = raf.readUTF();
                raf.skipBytes(12);
                if (alias.equals(aliased)){
                    raf.writeInt(newScorePerMinute);
                    raf.writeInt(newWins);
                    raf.writeInt(newLosses);
                    break;
                }
                raf.skipBytes(12);
            }
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(e.getLocalizedMessage());
        } catch (IOException e) {
            throw new IOException(e.getLocalizedMessage());
        }
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public static void main(String[] args) throws IOException {
        PlayerData playerData = new PlayerData();
        playerData.readData("src/main/resources/playerdata.bin");

        playerData.findPlayer("HunterKiller11111elf", 15000, 1337, 0);
        playerData.findPlayer("CyberBob", 2, 1, 354);
        playerData.findPlayer("ShadowDeath42",3,0,400);
        playerData.readData("src/main/resources/playerdata.bin");
        System.out.println(playerData.getPlayerList());
    }
}
